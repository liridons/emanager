﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eManager.Models
{

    public class Group
    {
        public int GroupId { get; set; }

        public string CompanyId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public GroupStatus Status { get; set; }
    }

    public enum GroupStatus
    {
        Active,
        Inactive,
        Private,
        Public
    }
}
