﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eManager.Models
{

    public class Holiday
    {
        public int HolidayId { get; set; }
        public string CompanyId { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }        
        public string Description { get; set; }
        public HolidayType HolidayType { get; set; }
        public HolidayStatus HolidayStatus { get; set; }
    }

    public enum HolidayStatus
    {
        Working,
        NonWorking,
        Partially
    }
    public enum HolidayType
    {
        National,
        Religious,
        CompanyDay,
        Other
    }
}
